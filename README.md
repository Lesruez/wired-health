# Wired Health Demo App

 https://demo093.web.app   <===>  Demo Web App link
## Installation
Git clone https://bitbucket.org/Lesruez/wired-health

then 
``` npm install  ```


 
Add cordova platforms android and browser

```ionic cordova platform add android```

```ionic cordova platform add browser```

```ionic cordova plugin add cordova-plugin-media```

then run
```ionic cordova prepare --prod```
### Well done you just configured the Ionic5 Wired Health  App

serve the app by running

``` npm run ionic:serve ```
