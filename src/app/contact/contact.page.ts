import { Component, OnInit } from '@angular/core';
import {ControllerService} from '../controller.service';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  number: any;
  email: any;
  first_name: any;
  last_name: any;


  constructor(
      private ctrl:ControllerService,
      private afs:AngularFirestore,) { }

  ngOnInit() {
  }

  save() {

    let data = {
      number:this.number,
      email:this.email,
      last_name:this.last_name,

      first_name:this.first_name
    }

    if (this.number && name && this.last_name && this.first_name && this.email){
      this.ctrl.presentLoader()
      this.afs.collection('contact').add(data).then(()=>{
        this.ctrl.presentToast("Success")
        this.ctrl.dismissLoader()
      }).catch(e=>{
        this.ctrl.dismissLoader()
        this.ctrl.presentToast('Something went wrong please try again')
      })
    }

    else {
      this.ctrl.presentToast("Fill all fields")
    }
  }

}
