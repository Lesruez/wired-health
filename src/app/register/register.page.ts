import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {ControllerService} from '../controller.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  password: any;
  email: any;
  name: any;

  constructor(
      private afAuth:AngularFireAuth,
      private ctrl:ControllerService,
      private navCtrl:NavController,
  ) { }

  ngOnInit() {
  }


  submit(){

    if (this.email && this.password){
        this.ctrl.presentLoader()
      this.afAuth.createUserWithEmailAndPassword(this.email,this.password).then(()=>{
        this.ctrl.presentToast("Done")
        this.navCtrl.navigateRoot('/')
        this.ctrl.dismissLoader()
      }).catch(error=>{
          let errorCode = error.code;
          this.ctrl.dismissLoader()

          let errorMessage = error.message;

          switch (errorCode) {

              case 'auth/weak-password': this.ctrl.presentToast('The password is too weak.');
                  //this.loading.dismiss()

                  break;

              case 'auth/invalid-email': this.ctrl.presentToast('Invalid Email.');
                  //  this.loading.dismiss()

                  break;

              case 'auth/operation-not-allowed': this.ctrl.presentToast('Operation not allowed');
                  //this.loading.dismiss()

                  break;


              case 'auth/email-already-in-use': this.ctrl.presentToast('Email already in use. Please login or reset your password ' +
                  '');
                  //   this.loading.dismiss()

                  break


              default :   this.ctrl.presentToast(errorMessage);
              // this.loading.dismiss()



          }

      })

    }else {
      this.ctrl.presentToast("Email or Password required")
    }

  }
}
