import { Component } from '@angular/core';

import {MenuController, NavController, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navCtrl:NavController,
    private afAuth:AngularFireAuth,
    private menu: MenuController){

   afAuth.authState.subscribe((res:any)=>{
     if (res && res.uid){
       navCtrl.navigateRoot('/')
     } else {
       navCtrl.navigateRoot('/landing')
     }
   })

    this.initializeApp();
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  closeMenu() {
    this.menu.close('first');
  }

  signOut() {
    this.menu.close('first')
    this.afAuth.signOut().then(()=>{
      this.navCtrl.navigateRoot('login')
    })

  }

  home() {
    this.closeMenu()
    this.navCtrl.navigateRoot('/')
  }
}
