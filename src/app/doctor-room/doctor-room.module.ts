import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DoctorRoomPageRoutingModule } from './doctor-room-routing.module';

import { DoctorRoomPage } from './doctor-room.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DoctorRoomPageRoutingModule
  ],
  declarations: [DoctorRoomPage]
})
export class DoctorRoomPageModule {}
