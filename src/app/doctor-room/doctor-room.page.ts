import { Component, OnInit } from '@angular/core';
import {ControllerService} from '../controller.service';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-doctor-room',
  templateUrl: './doctor-room.page.html',
  styleUrls: ['./doctor-room.page.scss'],
})
export class DoctorRoomPage implements OnInit {
  number: any;
  email: any;
  first_name: any;
  last_name: any;
  d_type: any;
  room_number: any;

  constructor(
      private ctrl:ControllerService,
      private afs:AngularFirestore,
  ) { }

  ngOnInit() {
  }

  save() {

    let data = {
      number:this.number,
      email:this.email,
      last_name:this.last_name,
      room_number:this.room_number,
      d_type:this.d_type,
      first_name:this.first_name
    }

    if (this.number && name && this.last_name && this.d_type && this.first_name && this.room_number && this.email){
      this.ctrl.presentLoader()
      this.afs.collection('doctor_room').add(data).then(()=>{
        this.ctrl.presentToast("Success")
        this.ctrl.dismissLoader()
      }).catch(e=>{
        this.ctrl.dismissLoader()
        this.ctrl.presentToast('Something went wrong please try again')
      })
    }
    
    else {
      this.ctrl.presentToast("Fill all fields")
    }
    }
   
}
