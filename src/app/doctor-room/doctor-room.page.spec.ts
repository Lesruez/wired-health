import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DoctorRoomPage } from './doctor-room.page';

describe('DoctorRoomPage', () => {
  let component: DoctorRoomPage;
  let fixture: ComponentFixture<DoctorRoomPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorRoomPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DoctorRoomPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
