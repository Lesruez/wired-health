import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DoctorRoomPage } from './doctor-room.page';

const routes: Routes = [
  {
    path: '',
    component: DoctorRoomPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoctorRoomPageRoutingModule {}
