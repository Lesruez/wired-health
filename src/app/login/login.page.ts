import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {error} from 'selenium-webdriver';
import {ControllerService} from '../controller.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: any;
  password: any;

  constructor(
      private afAuth:AngularFireAuth,
      private ctrl:ControllerService,
      private navCtrl:NavController,
  ) { }

  ngOnInit() {
  }


  submit(){

    if (this.email && this.password){
      this.ctrl.presentLoader()
      this.afAuth.signInWithEmailAndPassword(this.email,this.password).then(()=>{
        this.navCtrl.navigateRoot('/')
this.ctrl.dismissLoader()
      }).catch(e=> {

        this.ctrl.dismissLoader()
        if (e.code == 'auth/wrong-password') {
          this.ctrl.presentToast('Incorrect Password ');

        } else if (e.code == 'auth/invalid-email') {
          this.ctrl.presentToast('Invalid Email');

        } else if (e.code == 'auth/user-not-found') {
          this.ctrl.presentToast('User not found. Please Sign-up First');

        } else if (e.code == 'auth/user-disabled') {
          this.ctrl.presentToast('User currently disabled');

        } else {
          this.ctrl.presentToast('Something went wrong please try again.');

        }


      })
    }else {
      this.ctrl.presentToast("Email or Password required")
    }

  }
}
