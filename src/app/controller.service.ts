import { Injectable } from '@angular/core';
import {LoadingController, ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ControllerService {
  private loader: any;

  constructor(
    private  toastCtl:ToastController,
    private loading:LoadingController,
  ) { }



 async presentToast(msg){
  let toast = await this.toastCtl.create({
  message: msg,
  duration:3000,
  position: 'top'
})
    await  toast.present()

  }


 async presentLoader(){
    this.loader =  await this.loading.create({
      message: "Please wait ...",
      duration:3000,

    })
   await  this.loader.present()
  }

  dismissLoader(){

this.loader.dismiss().catch()
  }
}
